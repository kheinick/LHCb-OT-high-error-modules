#! /usr/bin/env python
from glob import glob
from tqdm import tqdm
import numpy as np

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from rootpy.io import root_open
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', help='''directory containing
                        EOR-root files. This should be `/hist/Savesets/2017/LHCb/OTDAQMon/09`
                        for example.''')
    return parser.parse_args()


def extract_errors(filelist):
    # placeholder dict to store bad modules
    bad_modules = {}

    # module ids per quarter
    module_ids = np.arange(1, 109)
    # module ids for all stations
    ys = np.zeros(4 * 108)
    for fname in tqdm(filelist):
        with root_open(fname) as file:
            run = 'placeholder'  # TODO: extract run number
            for q in range(4):
                hist = file.OTErrorBankMonitoring['ProblematicLink_$Q{q}'.format(q=q)]
                bins = np.asarray([hist.at(int(i)) for i in module_ids])
                selection = bins > 3 * bins.std()

                # if more then ~30% of the modules appear with high errors,
                # this might be another problem
                if selection.sum() > 30:
                    continue

                for mid in module_ids[selection]:
                    mkey = 'Q{q}M{mid}'.format(q=q, mid=mid)
                    mstat = bad_modules.get(mkey, {})
                    mstat['counter'] = mstat.get('counter', 0) + 1
                    ys[q * 108 + mid - 1] = mstat['counter']
                    mstat['runs'] = mstat.get('runs', []) + [run]
                    bad_modules[mkey] = mstat

    return ys, bad_modules


def plot_bad_modules(bad_modules):
    entries = np.asarray([[q, m, bad_modules['Q{q}M{m}'.format(q=q, m=m)]['counter']]
                          for q in range(4)
                          for m in range(1, 109)
                          if 'Q{q}M{m}'.format(q=q, m=m) in bad_modules])
    q, m, weight = entries.T

    fig, ax = plt.subplots()

    cmap = plt.get_cmap('rainbow')
    cmap.set_under('white')
    _, xedges, _, img = plt.hist2d(m, q, weights=weight,
                                   range=((0.5, 108.5), (-0.5, 3.5)), bins=(108, 4),
                                   cmap=cmap, vmin=0.1)
    plt.colorbar(img, ax=ax)
    plt.vlines(np.linspace(1, 108, 108), *plt.ylim(), colors='k', linestyles=':', alpha=0.5)
    plt.yticks(np.linspace(0, 3, 4), ['Q{q}'.format(q=q) for q in range(4)])
    plt.xticks(np.arange(1, 108, 3),
               ['T{t}L{l}M{m}'.format(t=t, l=l, m=m)
                for t in range(1, 4)
                for l in range(4)
                for m in range(1, 10, 3)],
               rotation='vertical')
    plt.title('OT Modules with peaking error rates')
    plt.tight_layout()
    plt.savefig('bad_modules.pdf')


if __name__ == '__main__':
    args = parse_args()
    plt.rc('figure', figsize=(20, 10))
    # get all the end-of-run root files (got them from /hist/Savesets/2017/LHCb/OTDAQMon/09)
    filelist = glob('{}/***EOR.root'.format(args.directory))
    _, bad_modules = extract_errors(filelist)
    plot_bad_modules(bad_modules)
